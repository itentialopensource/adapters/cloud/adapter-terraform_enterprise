## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Terraform Enterprise. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Terraform Enterprise.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Terraform Enterprise. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">get(callback)</td>
    <td style="padding:15px">GetAccountDetails</td>
    <td style="padding:15px">{base_path}/{version}/account/details?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAccount(body, callback)</td>
    <td style="padding:15px">Update Account</td>
    <td style="padding:15px">{base_path}/{version}/account/update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">changePassword(body, callback)</td>
    <td style="padding:15px">Change Password</td>
    <td style="padding:15px">{base_path}/{version}/account/password?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRun(body, callback)</td>
    <td style="padding:15px">Create a Run</td>
    <td style="padding:15px">{base_path}/{version}/runs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applyRun(runId, body, callback)</td>
    <td style="padding:15px">Apply a Run</td>
    <td style="padding:15px">{base_path}/{version}/runs/{pathv1}/actions/apply?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listRuns(workspaceId, pageNumber, pageSize, callback)</td>
    <td style="padding:15px">List Runs in a Workspace</td>
    <td style="padding:15px">{base_path}/{version}/workspaces/{pathv1}/runs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRunDetails(runId, callback)</td>
    <td style="padding:15px">Get run details</td>
    <td style="padding:15px">{base_path}/{version}/runs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">discardRun(runId, body, callback)</td>
    <td style="padding:15px">Discard a Run</td>
    <td style="padding:15px">{base_path}/{version}/runs/{pathv1}/actions/discard?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cancelRun(runId, body, callback)</td>
    <td style="padding:15px">Cancel a Run</td>
    <td style="padding:15px">{base_path}/{version}/runs/{pathv1}/actions/cancel?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">forceCancel(runId, body, callback)</td>
    <td style="padding:15px">Forcefully cancel a run</td>
    <td style="padding:15px">{base_path}/{version}/runs/{pathv1}/actions/force-cancel?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">forceExecute(runId, callback)</td>
    <td style="padding:15px">Forcefully execute a run</td>
    <td style="padding:15px">{base_path}/{version}/runs/{pathv1}/actions/force-execute?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApply(id, callback)</td>
    <td style="padding:15px">Get Apply</td>
    <td style="padding:15px">{base_path}/{version}/applies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCostEstimate(id, callback)</td>
    <td style="padding:15px">Get Cost Estimate</td>
    <td style="padding:15px">{base_path}/{version}/cost-estimates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNotificationConfiguration(body, callback)</td>
    <td style="padding:15px">Create Notification Configuration</td>
    <td style="padding:15px">{base_path}/{version}/workspaces/{pathv1}/notification-configurations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listNotificationConfigurations(workspaceId, callback)</td>
    <td style="padding:15px">List Notification Configurations</td>
    <td style="padding:15px">{base_path}/{version}/workspaces/{pathv1}/notification-configurations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNotificationConfiguration(notificationConfigurationId, callback)</td>
    <td style="padding:15px">Get Notification Configuration</td>
    <td style="padding:15px">{base_path}/{version}/notification-configurations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNotificationConfiguration(notificationConfigurationId, callback)</td>
    <td style="padding:15px">Update Notification Configuration</td>
    <td style="padding:15px">{base_path}/{version}/notification-configurations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNotificationConfiguration(notificationConfigurationId, callback)</td>
    <td style="padding:15px">Delete Notification Configuration</td>
    <td style="padding:15px">{base_path}/{version}/notification-configurations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">verifyNotificationConfiguration(notificationConfigurationId, callback)</td>
    <td style="padding:15px">Verify a Notification Configuration</td>
    <td style="padding:15px">{base_path}/{version}/notification-configurations/{pathv1}/actions/verify?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listOrganizations(callback)</td>
    <td style="padding:15px">List Organizations</td>
    <td style="padding:15px">{base_path}/{version}/organizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrganization(organizationName, callback)</td>
    <td style="padding:15px">Get Organization</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inviteUserToOrganization(organizationName, body, callback)</td>
    <td style="padding:15px">Invite a User to an Organization</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/organization-memberships?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listMembershipsOrganization(organizationName, q, filterStatus, pageNumber, pageSize, callback)</td>
    <td style="padding:15px">List Memberships for an Organization</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/organization-memberships?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listUserOwnMemberships(callback)</td>
    <td style="padding:15px">List User's Own Memberships</td>
    <td style="padding:15px">{base_path}/{version}/organization-memberships?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showMembership(organizationMembershipId, callback)</td>
    <td style="padding:15px">Show a Membership</td>
    <td style="padding:15px">{base_path}/{version}/organization-memberships/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeUserFromOrganization(organizationMembershipId, callback)</td>
    <td style="padding:15px">Remove User from Organization</td>
    <td style="padding:15px">{base_path}/{version}/organization-memberships/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showPlan(id, callback)</td>
    <td style="padding:15px">Show a plan</td>
    <td style="padding:15px">{base_path}/{version}/plans/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPolicy(organizationName, body, callback)</td>
    <td style="padding:15px">Create a Policy</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listPolicies(organizationName, pageNumber, pageSize, searchName, callback)</td>
    <td style="padding:15px">List Policies</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showPolicy(policyId, callback)</td>
    <td style="padding:15px">Show a Policy</td>
    <td style="padding:15px">{base_path}/{version}/policies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePolicy(policyId, body, callback)</td>
    <td style="padding:15px">Update a Policy</td>
    <td style="padding:15px">{base_path}/{version}/policies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePolicy(policyId, callback)</td>
    <td style="padding:15px">Delete a Policy</td>
    <td style="padding:15px">{base_path}/{version}/policies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">uploadPolicy(policyId, callback)</td>
    <td style="padding:15px">Upload a Policy</td>
    <td style="padding:15px">{base_path}/{version}/policies/{pathv1}/upload?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listPolicyChecks(runId, callback)</td>
    <td style="padding:15px">List policy checks</td>
    <td style="padding:15px">{base_path}/{version}/runs/{pathv1}/policy-checks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">overridePolicy(policyCheckId, callback)</td>
    <td style="padding:15px">Override Policy</td>
    <td style="padding:15px">{base_path}/{version}/policy-checks/{pathv1}/actions/override?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPolicySet(organizationName, body, callback)</td>
    <td style="padding:15px">Create a Policy Set</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/policy-sets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listPolicySets(organizationName, filterVersioned, include, pageNumber, pageSize, searchName, callback)</td>
    <td style="padding:15px">List Policy Sets</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/policy-sets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showPolicySet(id, callback)</td>
    <td style="padding:15px">Show a Policy Set</td>
    <td style="padding:15px">{base_path}/{version}/policy-sets/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePolicySet(id, body, callback)</td>
    <td style="padding:15px">Update a Policy Set</td>
    <td style="padding:15px">{base_path}/{version}/policy-sets/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePolicySet(id, callback)</td>
    <td style="padding:15px">Delete a Policy Set</td>
    <td style="padding:15px">{base_path}/{version}/policy-sets/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addPoliciesToPolicySet(id, body, callback)</td>
    <td style="padding:15px">Add Policies to the Policy Set</td>
    <td style="padding:15px">{base_path}/{version}/policy-sets/{pathv1}/relationships/policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removePoliciesFromThePolicySet(id, callback)</td>
    <td style="padding:15px">Remove Policies from the Policy Set</td>
    <td style="padding:15px">{base_path}/{version}/policy-sets/{pathv1}/relationships/policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">attachPolicySetToWorkspaces(id, body, callback)</td>
    <td style="padding:15px">Attach a Policy Set to workspaces</td>
    <td style="padding:15px">{base_path}/{version}/policy-sets/{pathv1}/relationships/workspaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">detachthePolicySetFromWorkspaces(id, callback)</td>
    <td style="padding:15px">Detach the Policy Set from workspaces</td>
    <td style="padding:15px">{base_path}/{version}/policy-sets/{pathv1}/relationships/workspaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPolicySetVersion(id, callback)</td>
    <td style="padding:15px">Create a Policy Set Version</td>
    <td style="padding:15px">{base_path}/{version}/policy-sets/{pathv1}/versions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showPolicySetVersion(id, callback)</td>
    <td style="padding:15px">Show a Policy Set Version</td>
    <td style="padding:15px">{base_path}/{version}/policy-set-versions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createParameter(policySetId, body, callback)</td>
    <td style="padding:15px">Create a Parameter</td>
    <td style="padding:15px">{base_path}/{version}/policy-sets/{pathv1}/parameters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listParameters(policySetId, callback)</td>
    <td style="padding:15px">List Parameters</td>
    <td style="padding:15px">{base_path}/{version}/policy-sets/{pathv1}/parameters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateParameters(policySetId, parameterId, body, callback)</td>
    <td style="padding:15px">Update Parameters</td>
    <td style="padding:15px">{base_path}/{version}/policy-sets/{pathv1}/parameters/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteParameters(policySetId, parameterId, callback)</td>
    <td style="padding:15px">Delete Parameters</td>
    <td style="padding:15px">{base_path}/{version}/policy-sets/{pathv1}/parameters/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createStateVersion(workspaceId, body, callback)</td>
    <td style="padding:15px">Create a State Version</td>
    <td style="padding:15px">{base_path}/{version}/workspaces/{pathv1}/state-versions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listStateVersions(filterWorkspaceName, filterOrganizationName, pageNumber, pageSize, callback)</td>
    <td style="padding:15px">List State Versions</td>
    <td style="padding:15px">{base_path}/{version}/state-versions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">fetchCurrentStateVersionForWorkspace(workspaceId, callback)</td>
    <td style="padding:15px">Fetch Current State Version</td>
    <td style="padding:15px">{base_path}/{version}/workspaces/{pathv1}/current-state-version?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showStateVersion(stateVersionId, callback)</td>
    <td style="padding:15px">Show a State Version</td>
    <td style="padding:15px">{base_path}/{version}/state-versions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showStateVersionOutput(stateVersionOutputId, callback)</td>
    <td style="padding:15px">Show a State Version Output</td>
    <td style="padding:15px">{base_path}/{version}/state-version-outputs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createVariable(body, callback)</td>
    <td style="padding:15px">Create a Variable</td>
    <td style="padding:15px">{base_path}/{version}/vars?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listVariables(filterWorkspaceName, filterOrganizationName, callback)</td>
    <td style="padding:15px">List Variables</td>
    <td style="padding:15px">{base_path}/{version}/vars?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateVariables(variableId, body, callback)</td>
    <td style="padding:15px">Update Variables</td>
    <td style="padding:15px">{base_path}/{version}/vars/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVariables(variableId, callback)</td>
    <td style="padding:15px">Delete Variables</td>
    <td style="padding:15px">{base_path}/{version}/vars/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createWorkspace(organizationName, body, callback)</td>
    <td style="padding:15px">Create a Workspace</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/workspaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listWorkspaces(organizationName, pageNumber, pageSize, callback)</td>
    <td style="padding:15px">List Workspaces</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/workspaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showWorkspace(workspaceId, callback)</td>
    <td style="padding:15px">Show Workspace</td>
    <td style="padding:15px">{base_path}/{version}/workspaces/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">lockWorkspace(workspaceId, callback)</td>
    <td style="padding:15px">Lock a workspace</td>
    <td style="padding:15px">{base_path}/{version}/workspaces/{pathv1}/actions/lock?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unlockWorkspace(workspaceId, callback)</td>
    <td style="padding:15px">Unlock a workspace</td>
    <td style="padding:15px">{base_path}/{version}/workspaces/{pathv1}/actions/unlock?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">forceUnlockWorkspace(workspaceId, callback)</td>
    <td style="padding:15px">Force Unlock a workspace</td>
    <td style="padding:15px">{base_path}/{version}/workspaces/{pathv1}/actions/force-unlock?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignSSHKeyToWorkspace(workspaceId, body, callback)</td>
    <td style="padding:15px">Assign/Unassign an SSH key to a workspace</td>
    <td style="padding:15px">{base_path}/{version}/workspaces/{pathv1}/relationships/ssh-key?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createWorkspaceVariable(workspaceId, body, callback)</td>
    <td style="padding:15px">Create a Workspace Variable</td>
    <td style="padding:15px">{base_path}/{version}/workspaces/{pathv1}/vars?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listWorkspaceVariables(workspaceId, callback)</td>
    <td style="padding:15px">List Workspace Variables</td>
    <td style="padding:15px">{base_path}/{version}/workspaces/{pathv1}/vars?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateWorkspaceVariables(workspaceId, variableId, body, callback)</td>
    <td style="padding:15px">Update Workspace Variables</td>
    <td style="padding:15px">{base_path}/{version}/workspaces/{pathv1}/vars/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteWorkspaceVariables(workspaceId, variableId, callback)</td>
    <td style="padding:15px">Delete Workspace Variables</td>
    <td style="padding:15px">{base_path}/{version}/workspaces/{pathv1}/vars/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listOauthClients(organizationName, callback)</td>
    <td style="padding:15px">List OAuth Clients</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/oauth-clients?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOAuthClient(organizationName, body, callback)</td>
    <td style="padding:15px">Create an OAuth Client</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/oauth-clients?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showOAuthClient(id, callback)</td>
    <td style="padding:15px">Show an OAuth Client</td>
    <td style="padding:15px">{base_path}/{version}/oauth-clients/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateOAuthClient(id, body, callback)</td>
    <td style="padding:15px">Update an OAuth Client</td>
    <td style="padding:15px">{base_path}/{version}/oauth-clients/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">destroyOAuthClient(id, callback)</td>
    <td style="padding:15px">Destroy an OAuth Client</td>
    <td style="padding:15px">{base_path}/{version}/oauth-clients/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listOAuthTokens(oauthClientId, callback)</td>
    <td style="padding:15px">List OAuth Tokens</td>
    <td style="padding:15px">{base_path}/{version}/oauth-clients/{pathv1}/oauth-tokens?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showOAuthToken(id, callback)</td>
    <td style="padding:15px">Show an OAuth Token</td>
    <td style="padding:15px">{base_path}/{version}/oauth-tokens/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateOAuthToken(id, body, callback)</td>
    <td style="padding:15px">Update an OAuth Token</td>
    <td style="padding:15px">{base_path}/{version}/oauth-tokens/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">destroyOAuthToken(id, callback)</td>
    <td style="padding:15px">Destroy an OAuth Token</td>
    <td style="padding:15px">{base_path}/{version}/oauth-tokens/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateOrganizationToken(organizationName, callback)</td>
    <td style="padding:15px">Generate a new organization token</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/authentication-token?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOrganizationToken(organizationName, callback)</td>
    <td style="padding:15px">Delete the organization token</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/authentication-token?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPlanExport(body, callback)</td>
    <td style="padding:15px">Create a plan export</td>
    <td style="padding:15px">{base_path}/{version}/plan-exports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showPlanExport(id, callback)</td>
    <td style="padding:15px">Show a plan export</td>
    <td style="padding:15px">{base_path}/{version}/plan-exports/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExportedPlanData(id, callback)</td>
    <td style="padding:15px">Delete exported plan data</td>
    <td style="padding:15px">{base_path}/{version}/plan-exports/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">downloadPlanExport(id, callback)</td>
    <td style="padding:15px">Download exported plan data</td>
    <td style="padding:15px">{base_path}/{version}/plan-exports/{pathv1}/download?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showUser(user, callback)</td>
    <td style="padding:15px">Show a User</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
