
## 0.4.4 [10-15-2024]

* Changes made at 2024.10.14_21:11PM

See merge request itentialopensource/adapters/adapter-terraform_enterprise!19

---

## 0.4.3 [09-18-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-terraform_enterprise!17

---

## 0.4.2 [08-14-2024]

* Changes made at 2024.08.14_19:25PM

See merge request itentialopensource/adapters/adapter-terraform_enterprise!16

---

## 0.4.1 [08-07-2024]

* Changes made at 2024.08.06_21:07PM

See merge request itentialopensource/adapters/adapter-terraform_enterprise!15

---

## 0.4.0 [07-17-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/cloud/adapter-terraform_enterprise!14

---

## 0.3.7 [03-28-2024]

* Changes made at 2024.03.28_13:25PM

See merge request itentialopensource/adapters/cloud/adapter-terraform_enterprise!13

---

## 0.3.6 [03-21-2024]

* Changes made at 2024.03.21_13:56PM

See merge request itentialopensource/adapters/cloud/adapter-terraform_enterprise!12

---

## 0.3.5 [03-15-2024]

* Update metadata.json

See merge request itentialopensource/adapters/cloud/adapter-terraform_enterprise!11

---

## 0.3.4 [03-11-2024]

* Changes made at 2024.03.11_15:37PM

See merge request itentialopensource/adapters/cloud/adapter-terraform_enterprise!10

---

## 0.3.3 [02-28-2024]

* Changes made at 2024.02.28_11:52AM

See merge request itentialopensource/adapters/cloud/adapter-terraform_enterprise!9

---

## 0.3.2 [12-26-2023]

* update axios and metadata

See merge request itentialopensource/adapters/cloud/adapter-terraform_enterprise!8

---

## 0.3.1 [12-14-2023]

* Remediation Merge Request

See merge request itentialopensource/adapters/cloud/adapter-terraform_enterprise!6

---

## 0.3.0 [12-14-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/cloud/adapter-terraform_enterprise!5

---

## 0.2.1 [04-06-2023]

* Utils version has been updated in package.json, and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/cloud/adapter-terraform_enterprise!4

---

## 0.2.0 [05-23-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/cloud/adapter-terraform_enterprise!3

---

## 0.1.4 [03-15-2021]

- Migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/cloud/adapter-terraform_enterprise!2

---

## 0.1.3 [07-10-2020]

- Update to the latest adapter foundation

See merge request itentialopensource/adapters/cloud/adapter-terraform_enterprise!1

---

## 0.1.2 [03-26-2020] & 0.1.1 [03-26-2020]

- Initial Commit

See commit 52f2e6d

---
