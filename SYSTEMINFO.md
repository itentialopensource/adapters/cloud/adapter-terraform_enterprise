# Terraform Enterprise

Vendor: Terraform
Homepage: https://www.terraform.io/

Product: Terraform Enterprise
Product Page: https://developer.hashicorp.com/terraform/enterprise

## Introduction
We classify Terraform Enterprise into the Cloud domain as Terraform Enterprise provides the capability to manage infrastructure in cloud environments.

## Why Integrate
The Terraform Enterprise adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Terraform Enterprise. With this adapter you have the ability to perform operations such as:

- Installation Resources in the Cloud: Resources can be automatically uploaded to the cloud.

- Add, Manage, and Remove Devices: When Itential turns up a new device on the network, it can add the device to the cloud, request that Terraform Enterprise discover the new device, or remove the device.

## Additional Product Documentation
The [API documents for Terraform Enterprise](https://developer.hashicorp.com/terraform/enterprise/api-docs)