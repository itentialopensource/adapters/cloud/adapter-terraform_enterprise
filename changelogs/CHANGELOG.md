
## 0.2.1 [04-06-2023]

* Utils version has been updated in package.json, and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/cloud/adapter-terraform_enterprise!4

---

## 0.2.0 [05-23-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/cloud/adapter-terraform_enterprise!3

---

## 0.1.4 [03-15-2021]

- Migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/cloud/adapter-terraform_enterprise!2

---

## 0.1.3 [07-10-2020]

- Update to the latest adapter foundation

See merge request itentialopensource/adapters/cloud/adapter-terraform_enterprise!1

---

## 0.1.2 [03-26-2020] & 0.1.1 [03-26-2020]

- Initial Commit

See commit 52f2e6d

---
